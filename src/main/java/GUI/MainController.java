package GUI;

import Model.Reservation;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.Optional;

public class MainController {
    @FXML
    private BorderPane mainBorderPanel;
    @FXML
    private TableColumn numerIDJan;

    @FXML
    private TableView<Reservation> tableJan;

    public void initialize() {
        numerIDJan.setCellValueFactory(new PropertyValueFactory<Reservation, String>("numer"));
        tableJan.getColumns().get(4).setStyle("-fx-background-color: red");
    }

    @FXML
    public void showAllClients() {
        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.initOwner(mainBorderPanel.getScene().getWindow());
        dialog.setTitle("All Clients");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("clients.fxml"));
        try {
            dialog.getDialogPane().setContent((Node) fxmlLoader.load());

        } catch (IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
            return;
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
//        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
    }

    public void showAllReservations() {
        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.initOwner(mainBorderPanel.getScene().getWindow());
        dialog.setTitle("All reservations");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("reservation.fxml"));
        try {
            dialog.getDialogPane().setContent((Node) fxmlLoader.load());

        } catch (IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
            return;
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
    }

    @FXML
    public void closeWindow() {
        Platform.exit();
    }
}
