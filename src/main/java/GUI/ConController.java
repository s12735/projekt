package GUI;

import GUI.datamodel.Client;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class ConController {

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField phoneNumberField;

    @FXML
    private TextField emailField;

    public Client getNewClient() {
        int clientID = 0;
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String phoneNumber = phoneNumberField.getText();
        String email = emailField.getText();

        Client newClient = new Client(clientID, firstName, lastName, phoneNumber, email);
        return newClient;
    }

    public void editClient(Client client) {
        firstNameField.setText(client.getFirstName());
        lastNameField.setText(client.getLastName());
        phoneNumberField.setText(client.getPhoneNumber());
        emailField.setText(client.getEmail());
    }

    public void updateClient(Client client) {
        client.setFirstName(firstNameField.getText());
        client.setLastName(lastNameField.getText());
        client.setPhoneNumber(phoneNumberField.getText());
        client.setEmail(emailField.getText());
    }

}















