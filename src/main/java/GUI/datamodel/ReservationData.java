package GUI.datamodel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Date;
import java.util.List;


public class ReservationData {

    private ObservableList<Reservation> reservations;
    private List<Model.Reservation> clis;

    public ReservationData() {
        reservations = FXCollections.observableArrayList();
    }

    public ObservableList<Reservation> getReservations() {
        return reservations;
    }

    public void addReservation(Reservation reservation, Client client) {
        Model.Reservation rez = new Model.Reservation();
        Model.Client cli = new Model.Client();

        rez.setReservationID(reservation.getReservationID());
        rez.setStartDate(new Date(reservation.getStartDate()));
        rez.setEndDate(new Date(reservation.getEndDate()));
//        rez.setReservationType(reservation.getReservationType());
        rez.setRoomID(reservation.getRoomNumber());

        cli.setClientID(client.getClientID());
        cli.setPhoneNumber(client.getPhoneNumber());
        cli.setFirstName(client.getFirstName());
        cli.setLastName(client.getLastName());
        cli.setEmail(client.getEmail());

        cli.addReservation(rez);
    }

    public void deleteReservation(Reservation item) {
        reservations.remove(item);
    }

    public void loadReservations() {
//        clis = DAO.getAllReservations();
//        Reservation c;
//        for (Model.Reservation cl : clis) {
//            c = new Reservation();
//            c.setReservationID(cl.getReservationID());
//            c.setEmail(cl.getEmail());
//            c.setFirstName(cl.getFirstName());
//            c.setLastName(cl.getLastName());
//            c.setPhoneNumber(cl.getPhoneNumber());
//            reservations.add(c);
//        }

    }


    public void saveReservation(Reservation reservation) {
//        Model.Reservation cli = new Model.Reservation();
//        cli.setReservationID(reservation.getReservationID());
//        cli.setFirstName(reservation.getFirstName());
//        cli.setLastName(reservation.getLastName());
//        cli.setPhoneNumber(reservation.getPhoneNumber());
//        cli.setEmail(reservation.getEmail());
//
//        System.out.println(reservation.getReservationID());
//        if (reservation.getReservationID() != 0)
//            DAO.updateReservation(cli);
//        else
//            DAO.addReservation(cli);
    }
}
