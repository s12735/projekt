package GUI.datamodel;

import DAO.DAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;


public class ClientData {

    private ObservableList<Client> clients;
    private List<Model.Client> clis;

    public ClientData() {
        clients = FXCollections.observableArrayList();
    }

    public ObservableList<Client> getClients() {
        return clients;
    }

    public void addClient(Client client) {
        System.out.println(client);
        Model.Client cli = new Model.Client();
        System.out.println("cos");
        cli.setFirstName(client.getFirstName());
        cli.setLastName(client.getLastName());
        cli.setPhoneNumber(client.getPhoneNumber());
        cli.setEmail(client.getEmail());
        DAO.addClient(cli);
    }

    public void deleteClient(Client item) {
        clients.remove(item);
    }

    public void loadClients() {
        clis = DAO.getAllClients();
        Client c;
        for (Model.Client cl : clis) {
            c = new Client();
            c.setClientID(cl.getClientID());
            c.setEmail(cl.getEmail());
            c.setFirstName(cl.getFirstName());
            c.setLastName(cl.getLastName());
            c.setPhoneNumber(cl.getPhoneNumber());
            clients.add(c);
        }

    }


    public void saveClient(Client client) {
        Model.Client cli = new Model.Client();
        cli.setClientID(client.getClientID());
        cli.setFirstName(client.getFirstName());
        cli.setLastName(client.getLastName());
        cli.setPhoneNumber(client.getPhoneNumber());
        cli.setEmail(client.getEmail());

        System.out.println(client.getClientID());
        if(client.getClientID() != 0)
            DAO.updateClient(cli);
        else
            DAO.addClient(cli);
}}
