package GUI.datamodel;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Reservation {
    private SimpleIntegerProperty reservationID = new SimpleIntegerProperty();
    private SimpleStringProperty startDate = new SimpleStringProperty("");
    private SimpleStringProperty endDate = new SimpleStringProperty("");
    private SimpleStringProperty reservationType = new SimpleStringProperty("");
    private SimpleStringProperty clientID = new SimpleStringProperty("");
    private SimpleStringProperty roomNumber;


    public Reservation() {
    }

    public int getReservationID() {
        return reservationID.get();
    }

    public SimpleIntegerProperty reservationIDProperty() {
        return reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID.set(reservationID);
    }

    public String getStartDate() {
        return startDate.get();
    }

    public SimpleStringProperty startDateProperty() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate.set(startDate);
    }

    public String getEndDate() {
        return endDate.get();
    }

    public SimpleStringProperty endDateProperty() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate.set(endDate);
    }

    public String getReservationType() {
        return reservationType.get();
    }

    public SimpleStringProperty reservationTypeProperty() {
        return reservationType;
    }

    public void setReservationType(String reservationType) {
        this.reservationType.set(reservationType);
    }

    public String getClientID() {
        return clientID.get();
    }

    public SimpleStringProperty clientIDProperty() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID.set(clientID);
    }

    public String getRoomNumber() {
        return roomNumber.get();
    }

    public SimpleStringProperty roomNumberProperty() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber.set(roomNumber);
    }

    public Reservation(int ReservationID, String firstName, String lastName, String phoneNumber, String email, String roomNumber) {
        this.reservationID.set(ReservationID);
        this.startDate.set(firstName);
        this.endDate.set(lastName);
        this.reservationType.set(phoneNumber);
        this.clientID.set(email);
        this.roomNumber.set(roomNumber);
    }

}
