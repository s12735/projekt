package GUI;

import Model.Reservation;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class RezController {

    @FXML
    private
    TextField reservationIDField;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private ChoiceBox<Reservation.ReservationType> reservationTypeBox;

    @FXML
    private TextField clientIDField;

    @FXML
    private TextField roomNumberField;

    public void initialize(){


        reservationTypeBox.setItems(FXCollections.<Reservation.ReservationType>observableArrayList(Reservation.ReservationType.values()));
        reservationTypeBox.setValue(Reservation.ReservationType.dodana);
    }
//    public Reservation getNewRez() {
//        int clientID = 0;
//        String firstName = firstNameField.getText();
//        String lastName = lastNameField.getText();
//        String phoneNumber = phoneNumberField.getText();
//        String email = emailField.getText();
//
//        Client newClient = new Client(clientID, firstName, lastName, phoneNumber, email);
//        return newClient;
//    }
//
//    public void editRez(Reservation reservation) {
//        clientIDField.setText(String.valueOf(client.getClientID()));
//        firstNameField.setText(client.getFirstName());
//        lastNameField.setText(client.getLastName());
//        phoneNumberField.setText(client.getPhoneNumber());
//        emailField.setText(client.getEmail());
//    }
//
//    public void updateRez(Reservation reservation) {
//        client.setClientID(Integer.parseInt(clientIDField.getText()));
//        client.setFirstName(firstNameField.getText());
//        client.setLastName(lastNameField.getText());
//        client.setPhoneNumber(phoneNumberField.getText());
//        client.setEmail(emailField.getText());
//    }

}















