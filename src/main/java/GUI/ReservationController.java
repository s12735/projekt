package GUI;

import GUI.datamodel.Client;
import GUI.datamodel.ClientData;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.Optional;

public class ReservationController {

    @FXML
    private BorderPane mainReservationPanel;

    @FXML
    private TableView<Client> contactsTable;

    private ClientData data;

    public void initialize() {
        data = new ClientData();
        data.loadClients();
        contactsTable.setItems(data.getClients());
    }

    @FXML
    public void showAddReservationDialog() {
        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.initOwner(mainReservationPanel.getScene().getWindow());
        dialog.setTitle("Add New Reservation");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("rezdialog.fxml"));
        try {
            dialog.getDialogPane().setContent((Node) fxmlLoader.load());

        } catch (IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            ConController conController = fxmlLoader.getController();
            Client newClient = conController.getNewClient();
            data.addClient(newClient);
        }
    }

    @FXML
    public void showEditReservationDialog() {
        Client selectedClient = contactsTable.getSelectionModel().getSelectedItem();
        if (selectedClient == null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("No Reservation Selected");
            alert.setHeaderText(null);
            alert.setContentText("Please select the contact you want to edit.");
            alert.showAndWait();
            return;
        }

        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.initOwner(mainReservationPanel.getScene().getWindow());
        dialog.setTitle("Edit Reservation");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("rezdialog.fxml"));
        try {
            dialog.getDialogPane().setContent((Node) fxmlLoader.load());
        } catch (IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        RezController rezController = fxmlLoader.getController();
//        rezController.edit(selectedClient);

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
//            rezController.update(selectedClient);
            data.saveClient(selectedClient);
        }


    }
}














