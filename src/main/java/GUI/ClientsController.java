package GUI;

import GUI.datamodel.Client;
import GUI.datamodel.ClientData;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.Optional;

public class ClientsController {



    @FXML
    private BorderPane mainClientsPanel;

    @FXML
    private TableView<Client> contactsTable;

    private ClientData data;

    public void initialize() {
        data = new ClientData();
        data.loadClients();
        contactsTable.getSelectionModel().setCellSelectionEnabled(true);
        contactsTable.setItems(data.getClients());
    }

    @FXML
    public void showAddContactDialog() {
        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.initOwner(mainClientsPanel.getScene().getWindow());
        dialog.setTitle("Add New Client");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("contactdialog.fxml"));
        try {
            dialog.getDialogPane().setContent((Node) fxmlLoader.load());

        } catch (IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            ConController conController = fxmlLoader.getController();
            Client newClient = conController.getNewClient();
            data.addClient(newClient);
            contactsTable.getItems().add(newClient);
        }
    }

    @FXML
    public void showEditContactDialog() {
        TablePosition pos = contactsTable.getSelectionModel().getSelectedCells().get(0);
        int row = pos.getRow();
        TableColumn col = pos.getTableColumn();
        String dat = (String) col.getCellObservableValue(contactsTable.getItems().get(row)).getValue();
        System.out.println(row + "  " + pos.getColumn());


        Client selectedClient = contactsTable.getSelectionModel().getSelectedItem();
        if (selectedClient == null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("No Client Selected");
            alert.setHeaderText(null);
            alert.setContentText("Please select the contact you want to edit.");
            alert.showAndWait();
            return;
        }

        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.initOwner(mainClientsPanel.getScene().getWindow());
        dialog.setTitle("Edit Client");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("contactdialog.fxml"));
        try {
            dialog.getDialogPane().setContent((Node) fxmlLoader.load());
        } catch (IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        ConController conController = fxmlLoader.getController();
        conController.editClient(selectedClient);

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            conController.updateClient(selectedClient);
            data.saveClient(selectedClient);
        }


    }
}














