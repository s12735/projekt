package email;

import java.io.File;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import Model.Client;
import Model.Reservation;

public class Email extends Thread {

	Reservation reservation;
	Client user;
	String smtp_auth, smtp_starttls_enable, smtp_host, smtp_port, username, password, senderName, reservationType;

	private  void sendEmail() {

		final String username = this.username;
		final String password = this.password;
		String email_message = "Witamy serdecznie!\n\n Państwa rezerwacja w Naszym obiekcie została "
				+ reservation.getReservationType() + ".";

		String email_recepient = user.getEmail();

		Properties props = new Properties();
		props.put("mail.smtp.auth", this.smtp_auth);
		props.put("mail.smtp.starttls.enable", this.smtp_starttls_enable);
		props.put("mail.smtp.host", this.smtp_host);
		props.put("mail.smtp.port", this.smtp_port);

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(this.username.split("\\@")[0], senderName));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email_recepient));
			String subject = "Rezerwacja została " + reservationType;
			message.setSubject(subject);
			message.setText(email_message);
			Transport.send(message);
			System.out.println("E-mail wysłany:" + user.getEmail() + " (rezerwacja " + reservationType + ")");
			this.interrupt();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		email();
		sendEmail();
	}

	private void email() {
		File fXmlFile = new File("config.dat");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		Document doc = null;
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		Element eElement = (Element) doc.getDocumentElement();
		smtp_auth = eElement.getElementsByTagName("smtp_auth").item(0).getTextContent();
		smtp_starttls_enable = eElement.getElementsByTagName("smtp_starttls_enable").item(0).getTextContent();
		smtp_host = eElement.getElementsByTagName("smtp_host").item(0).getTextContent();
		smtp_port = eElement.getElementsByTagName("smtp_port").item(0).getTextContent();
		username = eElement.getElementsByTagName("username").item(0).getTextContent();
		password = eElement.getElementsByTagName("password").item(0).getTextContent();
		senderName = eElement.getElementsByTagName("senderName").item(0).getTextContent();
	}

	public void send(Client user, Reservation reservation) {
		if (user.getEmail() != null) {
			this.reservation = reservation;
			this.reservationType = String.valueOf(reservation.getReservationType());
			this.user = user;
			this.start();
		}
	}
}
