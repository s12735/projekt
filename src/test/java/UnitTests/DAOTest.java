package UnitTests;

import org.junit.Before;
import org.junit.Test;

import DAO.DAO;
import Model.Client;
import junit.framework.Assert;

public class DAOTest {
	DAO dao = new DAO();
	Client user = new Client();
	
	@Before
	@Test
	public void addTest(){
		String firstName = "Adam";
		String lastName = "Ciska";
		String phoneNumber = "503412123";
		String email = "aciska@on.pl";
		
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setPhoneNumber(phoneNumber);
		
		Assert.assertTrue(dao.addClient(user));
	}

	@Test
	public void getTest(){
		Client user1 = new Client();
		user1 = dao.getClient(user);
		Assert.assertEquals(user1.getClientID(), user.getClientID());
		Assert.assertEquals(user1.getFirstName(), user.getFirstName());
		Assert.assertEquals(user1.getLastName(), user.getLastName());
		Assert.assertEquals(user1.getPhoneNumber(), user.getPhoneNumber());
	}
	
	@Test
	public void removeTest(){
		Assert.assertTrue(dao.deleteClient(user));
	}
}
